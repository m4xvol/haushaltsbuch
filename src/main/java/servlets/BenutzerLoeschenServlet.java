package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;

public class BenutzerLoeschenServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	DBManager dbm = new DBManager();
	RequestDispatcher rd;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int zuLoeschenderNutzer = Integer.parseInt(request.getParameter("nutzerid"));
		dbm.benutzerLoeschenID(zuLoeschenderNutzer);
		rd = request.getRequestDispatcher("/servlet/benutzer/laden");
		rd.forward(request, response);

	}
}
