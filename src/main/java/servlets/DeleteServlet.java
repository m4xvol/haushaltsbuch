package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;

public class DeleteServlet extends HttpServlet {

	DBManager dbm = new DBManager();
	RequestDispatcher rd;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int benutzerid = (Integer) request.getSession().getAttribute("id");
		int aufgerufeneSeite = (Integer)(request.getSession().getAttribute("seitenzahl"));


		switch (aufgerufeneSeite) {

		case 3:// User Loeschen
			int zuLoeschenderNutzer= Integer.parseInt(request.getParameter("nutzerid"));
			dbm.benutzerLoeschenID(zuLoeschenderNutzer);
			rd=request.getRequestDispatcher("/Admin.jsp");
			rd.forward(request, response);	
			
			
		case 5:// Kategorie Loeschen
			int zuLoeschendeKategorie=Integer.parseInt(request.getParameter("kategorieid"));
			dbm.kategorieLoeschen(zuLoeschendeKategorie, benutzerid);
			rd=request.getRequestDispatcher("/Kategorie.jsp");
			rd.forward(request, response);
			
			
		case 6:// Eintrag Loeschen
			int zuLoeschenderEintrag = Integer.parseInt(request.getParameter("eid"));
			dbm.eintragLoeschen(zuLoeschenderEintrag);
			rd=request.getRequestDispatcher("/Startseite.jsp");
			rd.forward(request, response);
			
		}
		
		
	}
}
