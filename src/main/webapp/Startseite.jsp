<%@ page import="java.util.*" %>
<%@ page import="dbzugriff.*" %>
<%@ page import="servlets.*" %>
<%@ page contentType="text/html; charset=UTF-8" %>


<header>
  <div align="center"><img src="/img/banner.png"> </div>
</header>
<head>
  <link rel="icon" type="image/png" href="/img/hdg_icon.png">
  <title>GDH - Ausgaben</title>
</head>

<body>
  <div id="menu">
    <nav>
      <ul>
        <li class="topmenue">
          <form method="POST" action="/servlet/eintrag/laden">
            <button type="submit" id="Startseite" name="Startseite">Startseite</button>
          </form>
        </li>

        <li class="topmenue">
          <a herf="#Bearbeitung"> Bearbeitung </a>
          <ul>
            <li class="submenu"><a href="/EintragAnlegen.jsp">Neuer Eintrag</a></li>
            <li class="submenu"><a href="/KategorieAnlegen.jsp">Neue Kategorie</a></li>
          </ul>
        </li>

        <li class="topmenue" style="margin-right:10% ;">
          <a href="#Profil"> <img src="/img/icons8-user-20.png">Profil </a>
          <ul>
            <li class="submenu">
              <form method="POST" action="/servlet/benutzer/abmelden">
                <button type="submit" id="Abmelden" name="Abmelden" style="margin-top:20px">Abmelden</button>
              </form>
            </li>
            <li class="class=submenu"><a href="/PWAendern.jsp">neues Passwort</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>


  <h2 style="font-family: monospace;">Ihre Kategorien:</h2>
  <table style="border: solid; ">
    <thead>
      <tr>
        <th>Kategorie</th>
        <th>Summe der Beträge</th>
        <th>Ändern</th>
        <th>Löschen</th>
      </tr>
    </thead>
    <%
        DBManager dbm = new DBManager();
        ArrayList<Kategorie> bestehendeKategorien = (ArrayList)request.getSession().getAttribute("Kategorien");
        for(Kategorie k : bestehendeKategorien){
          out.println("<tr><td>"+ k.getBezeichnung() +"</td><td>" + dbm.getSummeNachKategorie(k.getBezeichnung(), (Integer) request.getSession().getAttribute("id")) + "</td><td><form method='POST' action='/servlet/kategorie/aendern/laden'><button type='submit' id='kaendern'>!</button><input type='hidden' name='kategorieid' value='" + k.getkid() + "'></form></td> <td><form method='POST' action='/servlet/kategorie/loeschen'><button type='submit' id='kloeschen'>3</button><input type='hidden' name='kategorieid' value='" + k.getkid() + "'></form></td></tr>");
        }
        %>
  </table>
  </br>
  <h2 style="font-family: monospace;">Ihre Ausgaben:</h2>
  <table style="border: solid; ">
    <thead>
      <tr>
        <th>Datum</th>
        <th>Bezeichnung</th>
        <th>Preis in €</th>
        <th>Kategorie</th>
        <th>Ändern</th>
        <th>Löschen</th>
      </tr>
    </thead>
    <% 
        ArrayList<Eintrag> bestehendeEintraege = (ArrayList)request.getSession().getAttribute("Eintraege");
        for (Eintrag e : bestehendeEintraege) {
          out.println("<tr><td>"+ e.getAusgabedatum() +"</td><td>"+ e.getBezeichnung() + "</td><td>"+ e.getBetrag() + "</td><td>"+ dbm.getBezKategorie(e.getKategorieID()) + "</td><td><form method='POST' action='/servlet/eintrag/aendern/laden'><button type='submit' id='baendern'>!</button><input type='hidden' name='eintragid' value='" + e.getEid() + "'></form></td><td><form method='POST' action='/servlet/eintrag/loeschen'><button type='submit' id='bloeschen'>3</button><input type='hidden' name='eintragid' value='" + e.getEid() + "'></form></td></tr>");
        }
    %>
  </table>
  </br>
  <h2 style="font-family: monospace;">Nach Zeitraum anzeigen:</h2>
  <table>
      <form method="POST" action="/servlet/eintraege/nachdatum">
    <tr>
      <th>
          <input type="date" id="von" name="von" required>
      </th>
      <th>
        bis
      </th>
      <th>
          <input type="date" id="bis" name="bis" required>
      </th>
      <th>
          <button type="submit">Anzeigen</button>
      </th>
    </tr>
  </form>
</table>
</br>
<table>
  <thead>
    <tr>
      <th>Datum</th>
      <th>Bezeichnung</th>
      <th>Preis in €</th>
      <th>Kategorie</th>
    </tr>
  </thead>

  <% 
  if((ArrayList)request.getSession().getAttribute("eintraegeNachDatum")!=null){
  ArrayList<Eintrag> eintraegeInDatum = (ArrayList)request.getSession().getAttribute("eintraegeNachDatum");
  
    for(Eintrag ed : eintraegeInDatum){
      out.println("<tr><td>"+ed.getAusgabedatum()+"</td><td>"+ed.getBezeichnung()+"</td><td>"+ed.getBetrag()+"</td><td>"+dbm.getBezKategorie(ed.getKategorieID()) +"</td></tr>");
    }
  }
  if(request.getSession().getAttribute("antwort")!=null){
    out.println(request.getSession().getAttribute("antwort"));
  }

  ArrayList<Eintrag> leereTabelle = new ArrayList<Eintrag>();
  
  request.getSession().setAttribute("eintraegeNachDatum", leereTabelle);
  request.getSession().setAttribute("antwort", "");
    %>
</table>
  
  


  
    
    
    
    
 
</body>

<style>
  #kaendern {
    font-family: 'Wingdings 2';
  }

  #kloeschen {
    font-family: 'Wingdings 2';
  }

  #baendern {
    font-family: 'Wingdings 2';
  }

  #bloeschen {
    font-family: 'Wingdings 2';
  }

  li {
    list-style-type: none;
  }

  nav {
    width: 100vw;
    background-color: #D22118;
  }

  ul {
    margin: 0;
    padding: 0;
    display: flex;
  }

  li {
    list-style-type: none;
    margin: 0 2vw;
    font-size: 4vh;
  }

  a {
    text-decoration: none;
    color: white;
    padding: 2vw;
    font-family: monospace;
  }

  b {
    text-decoration: none;
    color: #D22118;
    padding: 2vw;
    font-family: monospace;
  }

  nav>ul>li {
    position: relative;
    display: inline-block;
  }

  nav>ul>li>ul {
    position: absolute;
    list-style-type: none;
    background-color: #D22118;
    display: none;
  }

  nav>ul>li:hover>ul {
    display: block;
  }

  a:hover {
    color: blue;
  }

  table td {
    border: 1px solid black;
  }

  th {
    border: 1px solid black;
  }

  button {
    background-color: D22118;
    color: white;
    font-size: 4vh;
    border: none;
    font-family: monospace;
  }

  button:hover {
    color: blue;
    cursor: pointer;
  }
</style>