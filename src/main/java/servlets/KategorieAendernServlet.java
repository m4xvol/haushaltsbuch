package servlets;

import java.io.IOException;
import java.time.LocalDate;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;

public class KategorieAendernServlet extends HttpServlet {
	int benutzerid;
	int kategorieid;
	DBManager dbm = new DBManager();
	RequestDispatcher rd;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	IOException{
		
		benutzerid=(Integer) request.getSession().getAttribute("id");
		kategorieid= (int) request.getSession().getAttribute("kategorieid");
		String bez = request.getParameter("kategorie");
	
		dbm.kategorieAendern(kategorieid, bez);
		rd=request.getRequestDispatcher("/servlet/eintrag/laden");
		rd.forward(request, response);
	}
}
