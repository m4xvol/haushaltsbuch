package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;
import dbzugriff.Eintrag;
import dbzugriff.Kategorie;

public class EintraegeLadenServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	int benutzerid;

	DBManager dbm = new DBManager();
	RequestDispatcher rd;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
	
		benutzerid = (Integer) request.getSession().getAttribute("id");

		if (benutzerid != -1) {
		
		//kategorien	
		benutzerid = (Integer) request.getSession().getAttribute("id");

		ArrayList<Kategorie> kategorie = new ArrayList<Kategorie>();
		kategorie = dbm.alleKategorienAusgeben(benutzerid);
		request.getSession().setAttribute("Kategorien", kategorie);
		//bis hier
			
		
		ArrayList<Eintrag> eintraege = new ArrayList<Eintrag>();
		eintraege = dbm.alleEintraegeAusgeben(benutzerid);
				
		request.getSession().setAttribute("Eintraege", eintraege);
		

		rd = request.getRequestDispatcher("/Startseite.jsp");
		rd.forward(request, response);

		}
		else {
			rd = request.getRequestDispatcher("/index.jsp");
			rd.forward(request, response);
		}
	}
}
