package servlets;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.*;

public class PasswortAendernServlet extends HttpServlet
{
	int benutzerid;
	DBManager dbm = new DBManager();
	RequestDispatcher rd;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	IOException{
		
		benutzerid = (Integer) request.getSession().getAttribute("id");
		String name = dbm.getNameZuId(benutzerid);
		
		String altespasswort = request.getParameter("altespwfeld");
		int altespwhash = altespasswort.hashCode();
		String passwort;
		String passwortkontrolle;
		passwort=request.getParameter("passwort");
		passwortkontrolle=request.getParameter("passwortkontrolle");
		
		String fehler = null;
			
		
		
		if(passwort.equals(passwortkontrolle))
		{
			if(altespwhash == dbm.passwortAbfragen(name)) {
			dbm.passwortAendern(benutzerid, passwort);
			rd=request.getRequestDispatcher("/Startseite.jsp");
			}else {
				fehler = "Ihr altes Passwort ist falsch.";
				request.setAttribute("ausgabe", fehler);
				rd=request.getRequestDispatcher("/PWAendern.jsp");
			}
		}else 
		{
			fehler="FEHLER!! "+
				 "Die eingegebenen Passwoerter stimmen nicht Ueberein. Bitte versuchen "+
		            "Sie es erneut.";
				
			request.setAttribute("ausgabe", fehler);
			rd=request.getRequestDispatcher("/PWAendern.jsp");
			
	}//else
		rd.forward(request, response);
}
}
