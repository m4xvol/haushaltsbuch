<%@ page import="java.util.*" %>
<%@ page import="dbzugriff.*" %>
<%@ page import="servlets.*" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<header>
        <div align="center"><img src="/img/banner.png"> </div>
</header>
<head>
        <link rel="icon" type="image/png" href="/img/hdg_icon.png">
        <title>GDH - Passwort ändern</title>
</head>

<body>
        <div id="menu">
                <nav>
                        <ul>
                                <li class="topmenue">
                                        <form method="POST" action="/servlet/eintrag/laden" id="startseiteform">
                                                <button 
                                                        type="submit" id="Startseite"
                                                        name="Startseite">Startseite</button>
                                        </form>
                                </li>

                                <li class="topmenue">
                                        <a herf="#Bearbeitung"> Bearbeitung </a>
                                        <ul>
                                                <li class="submenu"><a href="/EintragAnlegen.jsp">Neuer Eintrag</a></li>
                                                <li class="submenu"><a href="/KategorieAnlegen.jsp">Neue Kategorie</a>
                                                </li>
                                        </ul>
                                </li>

                                <li class="topmenue" style="margin-right:10% ;">
                                        <a href="#Profil"> <img src="/img/icons8-user-20.png">Profil </a>
                                        <ul>
                                                <li class="submenu">
                                                        <form method="POST" action="/servlet/benutzer/abmelden">
                                                                <button 
                                                                        type="submit" id="Abmelden" name="Abmelden"
                                                                        style="margin-top:20px">Abmelden</button>
                                                        </form>
                                                </li>
                                                <li class="class=submenu"><a href="/PWAendern.jsp">neues Passwort</a>
                                                </li>
                                        </ul>
                                </li>
                        </ul>
                </nav>
        </div>

        <form method="Post" action="${CP}/servlet/passwort/aendern">

                <label id="altespwlabel" for="altespwfeld">Altes Passwort:</br>
                        <input type="password" name="altespwfeld" placeholder="Altes Passwort" maxlength="30"
                                required></br>
                </label>

                <label id="neuespasswortlabel" for="passwort">Neues Passwort:</br>
                        <input type="password" name="passwort" placeholder="mindestens 8 Zeichen" minlength="5"
                                required></br>
                </label>
                <label id="passwortkontrollelabel" for="passwortkontrolle">Neues Passwort wiederholen:</br>
                        <input type="password" name="passwortkontrolle" placeholder="mindestens 8 Zeichen" minlength="5"
                                required></br>
                </label>
                <label id="fehlerausgabe"></label></br></br>

                <button style="background-color:D22118; color:white;  border-radius:20px; height: 30px; width: 100px; font-size:20px "
                        type="submit" id="Aendern" name="Aendern" style="margin-top:20px">Ändern</button>
                </div>
        </form>

</body>
<style>
        form {
                margin-left: 30%;
                margin-top: 30px;
                font-family: monospace;
                font-size: 4vh;
        }

        nav {
                width: 100vw;
                background-color: #D22118;
        }

        ul {
                margin: 0;
                padding: 0;
                display: flex;
        }

        li {
                list-style-type: none;
                margin: 0 2vw;
                font-size: 4vh;
        }

        a {
                text-decoration: none;
                color: white;
                padding: 2vw;
                font-family: monospace;
        }

        b {
                text-decoration: none;
                color: #D22118;
                padding: 2vw;
                font-family: monospace;
        }

        nav>ul>li {
                position: relative;
                display: inline-block;
        }

        nav>ul>li>ul {
                position: absolute;
                list-style-type: none;
                background-color: #D22118;
                display: none;
        }

        nav>ul>li:hover>ul {
                display: block;
        }

        a:hover {
                color: blue;
        }

        form#startseiteform {
                margin-top: 0%;
                margin-left: 0%;
        }

        button {
                background-color: D22118;
                color: white;
                font-size: 4vh;
                border: none;
                font-family: monospace;
        }

        button:hover {
                color: blue;
                cursor: pointer;
        }
</style>