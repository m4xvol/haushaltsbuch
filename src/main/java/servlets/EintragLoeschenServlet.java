package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;

public class EintragLoeschenServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	DBManager dbm = new DBManager();
	RequestDispatcher rd;
	int benutzerid;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		benutzerid = (Integer)request.getSession().getAttribute("id");
		
		if (benutzerid != -1) {

		int zuLoeschenderEintrag = Integer.parseInt(request.getParameter("eintragid"));
		dbm.eintragLoeschen(zuLoeschenderEintrag);
		rd = request.getRequestDispatcher("/servlet/eintrag/laden");
		rd.forward(request, response);

		}
		
		else {
			rd = request.getRequestDispatcher("/index.jsp");
			rd.forward(request, response);
		}
	}

}