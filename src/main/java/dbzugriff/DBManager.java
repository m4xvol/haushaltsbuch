package dbzugriff;

import java.nio.charset.Charset;
import java.sql.*;
import java.time.LocalDate;
import java.util.*;
import org.apache.commons.lang3.RandomStringUtils;

public class DBManager {

	public static void main(String[] args) {
	
		
	}

	// Hilfmethode
	public int getIDZuName(String benutzer) {
		int id = -1;

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "select uid from benutzer where name='" + benutzer + "';";

			Statement st = conn.createStatement();
			st.executeQuery(query);

			ResultSet result = st.executeQuery(query);
			while (result.next()) {
				id = Integer.parseInt(result.getString("uid"));
			}

		} // try
		catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		} // catch

		return id;
	}

	// getestet
	public void eintragLoeschen(int eid) {

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "DELETE from eintrag WHERE eid=" + eid + ";";
			Statement st = conn.createStatement();
			st.executeUpdate(query);

		} // try
		catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		} // catch
	}// getID

	// getestet
	public void eintragErstellen(String bez, double betrag, LocalDate ausgabedatum, int kategorie, int benutzer) {

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "Insert into eintrag(bez, betrag, ausgabedatum, erstelldatum, kategorie, benutzer) "
					+ "Values('" + bez + "','" + betrag + "','" + ausgabedatum + "','" + java.time.LocalDate.now()
					+ "','" + kategorie + "','" + benutzer + "');";
			Statement st = conn.createStatement();
			st.executeUpdate(query);

		} // try
		catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		} // catch
	}// eintragErstellen

	// getestet
	public void eintragAendern(int eid, String bez, double betrag, LocalDate ausgabedatum, int kategorie) {

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "UPDATE eintrag SET bez='" + bez + "', betrag=" + betrag + ", ausgabedatum='" + ausgabedatum
					+ "', kategorie=" + kategorie + " WHERE eid= " + eid + ";";
			Statement st = conn.createStatement();
			st.executeUpdate(query);

		} // try
		catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		} // catch
	}// eintragAendern

	// getestet
	public void kategorieErstellen(String bez, int benutzer) {

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "Insert into kategorie (bezeichnung,benutzer) Values('" + bez + "','" + benutzer + "');";
			Statement st = conn.createStatement();
			st.executeUpdate(query);

		} // try
		catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		} // catch
	}// kategorieErstellen

	// getestet
	public void kategorieLoeschen(int kid, int benutzer) {
		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			// erst alle Eintr�ge in Standartkategorie verschieben, dann Kategorie
			// l�schen
			String query = "Update eintrag Set kategorie=1 where kategorie=" + kid + " And benutzer=" + benutzer + ";";
			Statement st = conn.createStatement();
			st.executeUpdate(query);

			query = "Delete from kategorie where kid=" + kid + ";";
			st.executeUpdate(query);
		} // try
		catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		} // catch
	}// kategorieLoeschen

	// getestet
	public void kategorieAendern(int kid, String bez) {

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "Update kategorie Set bezeichnung='" + bez + "' where kid=" + kid + ";";
			Statement st = conn.createStatement();
			st.executeUpdate(query);

		} // try
		catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		} // catch
	}// kategorieAendern

	// getestet
	public ArrayList<Kategorie> alleKategorienAusgeben(int benu) {
		ArrayList<Kategorie> alleKategorienTab = new ArrayList<Kategorie>();
		int kid;
		String bezeichnung;
		int benutzer;

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "select * from kategorie where benutzer=" + benu + ";";

			Statement st = conn.createStatement();

			ResultSet result = st.executeQuery(query);

			while (result.next()) {
				kid = Integer.parseInt(result.getString("kid"));
				bezeichnung = result.getString("bezeichnung");
				benutzer = Integer.parseInt(result.getString("benutzer"));
				alleKategorienTab.add(new Kategorie(kid, bezeichnung, benutzer));
			}

		} // try
		catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		} // catch

		return alleKategorienTab;
	}// alleKategorienAusgeben

	// getestet
	public ArrayList<Eintrag> alleEintraegeAusgeben(int benu) {
		ArrayList<Eintrag> alleEintraegeTab = new ArrayList<Eintrag>();
		int eid;
		String bezeichnung;
		double betrag;
		LocalDate ausgabedatum;
		LocalDate erstelldatum;
		int kategorie;
		int benutzer;

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "select * from eintrag where benutzer=" + benu + ";";

			Statement st = conn.createStatement();

			ResultSet result = st.executeQuery(query);

			while (result.next()) {
				eid = Integer.parseInt(result.getString("eid"));
				bezeichnung = result.getString("bez");
				betrag = Double.parseDouble(result.getString("betrag"));
				ausgabedatum = LocalDate.parse(result.getString("ausgabedatum"));
				erstelldatum = LocalDate.parse(result.getString("erstelldatum"));
				kategorie = Integer.parseInt(result.getString("kategorie"));
				benutzer = Integer.parseInt(result.getString("benutzer"));
				alleEintraegeTab
						.add(new Eintrag(eid, bezeichnung, betrag, ausgabedatum, erstelldatum, kategorie, benutzer));
			}

		} // try
		catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		} // catch
		return alleEintraegeTab;
	}// alleEintraegeAusgeben

	// getestet
	public int passwortAbfragen(String name) {
		int passwort = -1;
		int uid = getIDZuName(name); // Kommt drauf an wie deine Methode hei�t

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "select passwort from benutzer where uid=" + uid + ";";

			Statement st = conn.createStatement();

			ResultSet result = st.executeQuery(query);

			while (result.next()) {
				passwort = Integer.parseInt(result.getString("passwort"));
			}

		} // try
		catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		} // catch
		return passwort;
	}// passwortAbfragen

	// getestet
	public ArrayList<Benutzer> alleBenutzerAusgeben() {

		ArrayList<Benutzer> alleBenutzerTab = new ArrayList<Benutzer>();
		int id;
		String name;
		String email;

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "select * from benutzer;";

			Statement st = conn.createStatement();

			st.executeQuery(query);

			ResultSet result = st.executeQuery(query);

			while (result.next()) {

				id = Integer.parseInt(result.getString("uid"));
				name = result.getString("name");
				email = result.getString("email");
				alleBenutzerTab.add(new Benutzer(id, name, email));

			}

		} catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		}

		return alleBenutzerTab;

	}

	// getestet
	public void benutzerAnlegen(String name, String passw, String mail, int admin) {
		int passwort = passw.hashCode();
		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "INSERT INTO benutzer (name, passwort, email, admin) VALUES ( '" + name + "' , '" + passwort
					+ "' , '" + mail + "' , '" + admin + "' );";

			Statement st = conn.createStatement();

			st.executeUpdate(query);

			conn.close();
		} catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		}

	}

	// getestet
	public ArrayList<Benutzer> getId(String name) {

		ArrayList<Benutzer> nameIdTab = new ArrayList<Benutzer>();

		int id;
		String n;

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "SELECT uid, name FROM benutzer WHERE name LIKE '%" + name + "%';";

			Statement st = conn.createStatement();

			st.executeQuery(query);

			ResultSet result = st.executeQuery(query);

			while (result.next()) {

				id = Integer.parseInt(result.getString(1));
				n = result.getString(2);
				nameIdTab.add(new Benutzer(id, n));

			}

		} catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		}

		return nameIdTab;

	}

	// getestet
	public boolean benutzerLoeschenID(int id) {

		boolean erfolgreich = false;

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "DELETE from benutzer where uid = " + id + ";";

			Statement st = conn.createStatement();

			st.executeUpdate(query);

		} catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		}

		return erfolgreich;

	}

	// getestet
	public boolean benutzerLoeschenName(String name) {

		boolean erfolgreich = false;

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "DELETE from benutzer where name = " + name + ";";

			Statement st = conn.createStatement();

			st.executeUpdate(query);

			erfolgreich = true;

		} catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		}

		return erfolgreich;

	}

	// getestet
	public String passwortZuruecksetzen(int id) {
		String neuesPw = "";
		String hashString;

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			neuesPw = randomPwGenerator(10, true, true);
			int pwhash = neuesPw.hashCode();
			hashString = ""+pwhash;

			String query = "Update benutzer set passwort = '" + hashString + "'  where uid =" + id + ";";

			Statement st = conn.createStatement();

			st.executeUpdate(query);


		} catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		}

		return neuesPw;

	}

	// getestet
	public boolean passwortAendern(int id, String neuesPw) {

		boolean erfolgreich = false;

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");
			int pwhash = neuesPw.hashCode();

			String query = "Update benutzer set passwort = '" + pwhash + "'  where uid =" + id + ";";

			Statement st = conn.createStatement();

			st.executeUpdate(query);

		} catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		}

		return erfolgreich;

	}

	// getestet
	public String randomPwGenerator(int laenge, boolean buchstaben, boolean zahlen) {

		String neuesPasswort = RandomStringUtils.random(laenge, buchstaben, zahlen);

		return neuesPasswort;

	}

	public int getIdKategorie(String bezKat, int benId) {

		int katId = 1;

		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "SELECT kid FROM kategorie WHERE bezeichnung = '" + bezKat + "' and benutzer = " + benId
					+ ";";

			Statement st = conn.createStatement();

			st.executeQuery(query);

			ResultSet result = st.executeQuery(query);

			while (result.next()) {

				katId = result.getInt(1);
			}

		} catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		}
		

		return katId;

	}
	
	public String getBezKategorie(int kid) {
		
		String bezeichnung = "";
		
		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "SELECT bezeichnung FROM kategorie WHERE kid = " + kid + ";" ;

			Statement st = conn.createStatement();

			st.executeQuery(query);

			ResultSet result = st.executeQuery(query);

			while (result.next()) {
				bezeichnung = result.getString("bezeichnung");
			}

		} catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		}
		
		return bezeichnung;
		
	}
	
	
	//getestet
public Eintrag getEintrag(int eid) {
		
		Eintrag e = null;
		
		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "SELECT * FROM eintrag WHERE eid = " + eid + ";" ;

			Statement st = conn.createStatement();

			st.executeQuery(query);

			ResultSet result = st.executeQuery(query);

			while (result.next()) {
				eid = Integer.parseInt(result.getString("eid"));
				String bezeichnung = result.getString("bez");
				double betrag = Double.parseDouble(result.getString("betrag"));
				LocalDate ausgabedatum = LocalDate.parse(result.getString("ausgabedatum"));
				LocalDate erstelldatum = LocalDate.parse(result.getString("erstelldatum"));
				int kategorie = Integer.parseInt(result.getString("kategorie"));
				int benutzer = Integer.parseInt(result.getString("benutzer"));
				e = new Eintrag(eid, bezeichnung, betrag, ausgabedatum, erstelldatum, kategorie, benutzer);
			}

		} catch (Exception d) {
			System.out.println("Verbindung gescheitert - Error" + d);
		}
		
		return e;
		
	}//getEintrag

	public String getNameZuId(int uid) {
		String name=null;
		
		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "select name from benutzer where uid=" + uid + ";";

			Statement st = conn.createStatement();
			st.executeQuery(query);

			ResultSet result = st.executeQuery(query);
			while (result.next()) {
				name = result.getString("name");
			}

		} // try
		catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		} // catch
		
		return name;
	}//getNameZuID
	
	public int istAdmin(String benutzer) {
		int admin = -1;
		
		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "select admin from benutzer where name='" + benutzer + "';";

			Statement st = conn.createStatement();
			st.executeQuery(query);

			ResultSet result = st.executeQuery(query);
			while (result.next()) {
				admin = result.getInt("admin");
			}

		} // try
		catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		} // catch
		
		return admin;
	}//istAdmin
	
	public String getEmailZuId(int id) {
		String email = null;
		
		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "select email from benutzer where uid='" + id + "';";

			Statement st = conn.createStatement();
			st.executeQuery(query);

			ResultSet result = st.executeQuery(query);
			while (result.next()) {
				email = result.getString("email");
			}

		} // try
		catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		} // catch
		
		return email;
	}
	
	public String getSummeNachKategorie(String kategorie, int benID) {
		
		int kategorieID = getIdKategorie(kategorie, benID);
		String summe = "";
		
		try {
			String myDriver = "com.mysql.cj.jdbc.Driver";
			String myUrl = "jdbc:mysql://localhost/gelddeshauses?serverTimezone=UTC";

			Class.forName(myDriver);
			Connection conn = DriverManager.getConnection(myUrl, "root", "");
			System.out.println("Verbindung mit Datenbank wurde aufgebaut");

			String query = "select sum(betrag) from eintrag where benutzer='" + benID + "' and kategorie = " + kategorieID + ";";

			Statement st = conn.createStatement();
			st.executeQuery(query);

			ResultSet result = st.executeQuery(query);
			while (result.next()) {
				summe = result.getString("sum(betrag)");
			}

		} // try
		catch (Exception e) {
			System.out.println("Verbindung gescheitert - Error" + e);
		} // catch
		
		return summe;
		
	}
}// class