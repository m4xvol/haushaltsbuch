package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;
import dbzugriff.Eintrag;
import dbzugriff.Kategorie;
import dbzugriff.Benutzer;


public class ReadServlet extends HttpServlet{
	
	int benutzerid;
	
	DBManager dbm =new DBManager();
	RequestDispatcher rd;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	IOException
	{
		benutzerid=(Integer) request.getSession().getAttribute("id");
		int aufgerufeneSeite = (Integer)(request.getSession().getAttribute("seitenzahl"));
		
		switch(aufgerufeneSeite)
		{
		case 6://Einträge laden
			ArrayList<Eintrag> eintraege = new ArrayList<Eintrag>();
			eintraege= dbm.alleEintraegeAusgeben(benutzerid);
			request.getSession().setAttribute("Eintraege", eintraege);
			
			 rd =request.getRequestDispatcher("/Startseite.jsp");
			 rd.forward(request, response);
			
		case 7://Kategorien laden
			ArrayList<Kategorie> kategorie = new ArrayList<Kategorie>();
			kategorie=dbm.alleKategorienAusgeben(benutzerid);
			request.getSession().setAttribute("Kategorien", kategorie);
			
			 rd = request.getRequestDispatcher("/KategorieEinsehen.jsp");
			 rd.forward(request, response);
			 
		case 3://Benutzer laden
			ArrayList<Benutzer> benutzer = new ArrayList<Benutzer>();
			benutzer= dbm.alleBenutzerAusgeben();
			request.getSession().setAttribute("Benutzer", benutzer);
			rd=request.getRequestDispatcher("/Admin.jsp");
			rd.forward(request, response);
			
		}
	}
	

}
