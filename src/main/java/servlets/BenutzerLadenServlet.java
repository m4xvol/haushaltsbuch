package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;
import dbzugriff.Benutzer;

public class BenutzerLadenServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	int benutzerid;

	DBManager dbm = new DBManager();
	RequestDispatcher rd;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		benutzerid = (Integer) request.getSession().getAttribute("id");

		ArrayList<Benutzer> benutzer = new ArrayList<Benutzer>();
		benutzer = dbm.alleBenutzerAusgeben();
		request.getSession().setAttribute("Benutzer", benutzer);
		rd = request.getRequestDispatcher("/Admin.jsp");
		rd.forward(request, response);
	}

}
