package dbzugriff;

import java.time.LocalDate;

public class Eintrag {
	private int eid = -1;
	private String bezeichnung = null;
	private double betrag = -1;
	private LocalDate ausgabedatum = null;
	private LocalDate erstelldatum = null;
	private int kategorie = 0;
	private int benutzer = -1;

	Eintrag() {
		
	}
	
	Eintrag(int eid, String bezeichnung, double betrag, LocalDate ausgabedatum, LocalDate erstellDatum, int kategorie,
			int benutzer) {
		this.eid = eid;
		this.bezeichnung = bezeichnung;
		this.betrag = betrag;
		this.ausgabedatum = ausgabedatum;
		this.erstelldatum = erstellDatum;
		this.kategorie = kategorie;
		this.benutzer = benutzer;
	}// Eintrag

	public int getEid() {
		return eid;
	}

	public String getBezeichnung() {
		return bezeichnung;
	}

	public double getBetrag() {
		return betrag;
	}

	public LocalDate getAusgabedatum() {
		return ausgabedatum;
	}

	public LocalDate getErstelldatum() {
		return erstelldatum;
	}

	public int getKategorieID() {
		
		return kategorie;		
	}

	public int getBenutzerID() {
		return benutzer;
	}
}// Eintrag
