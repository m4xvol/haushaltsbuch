package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;

public class KategorieLoeschenServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	DBManager dbm = new DBManager();
	RequestDispatcher rd;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int benutzerid = (Integer) request.getSession().getAttribute("id");

		int zuLoeschendeKategorie = Integer.parseInt(request.getParameter("kategorieid"));
		dbm.kategorieLoeschen(zuLoeschendeKategorie, benutzerid);
		rd = request.getRequestDispatcher("/servlet/eintrag/laden");
		rd.forward(request, response);

	}

}