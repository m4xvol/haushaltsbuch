package dbzugriff;

public class Benutzer {

	private int id = -1;
	private String name = null;
	private int passwort = -1;
	private String email = null;
	private int admin = -1;

	Benutzer(int i, String n, int p, String e, int a) {

		id = i;
		name = n;
		passwort = p;
		email = e;
		admin = a;

	}

	Benutzer(int i, String n) {

		id = i;
		name = n;

	}

	Benutzer(int i, String n, String e) {

		id = i;
		name = n;
		email = e;

	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getPasswort() {
		return passwort;
	}

	public String getEmail() {
		return email;
	}

	public int getAdmin() {
		return admin;
	}

}
