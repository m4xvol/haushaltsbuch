package servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;
import dbzugriff.Eintrag;
import dbzugriff.Kategorie;

public class KategorieAendernLadenServlet extends HttpServlet{
	
	int benutzerid;
	int kategorieid;
	String bez;
	String datumString;
	DBManager dbm = new DBManager();
	RequestDispatcher rd;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	IOException{
		benutzerid=(Integer) request.getSession().getAttribute("id");
		kategorieid = Integer.parseInt(request.getParameter("kategorieid"));
		request.getSession().setAttribute("kategorieid", kategorieid);
		
		if (benutzerid != -1) {
		
		bez = dbm.getBezKategorie(kategorieid);
		
		request.getSession().setAttribute("kategoriebez", bez);
		
		rd=request.getRequestDispatcher("/KategorieAendern.jsp");
		rd.forward(request, response);
		
		}
		else {
			rd = request.getRequestDispatcher("/index.jsp");
			rd.forward(request, response);
		}
	}

}