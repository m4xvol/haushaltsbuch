<%@ page import="java.util.*" %>
<%@ page import="dbzugriff.*" %>
<%@ page import="servlets.*" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<head>
    <link rel="icon" type="image/png" href="/img/hdg_icon.png">
    <title>HDG - Registrieren</title>
</head>
<header>
    <div align="center"><img src="/img/banner.png"> </div>
    <% request.getSession().setAttribute("seitenzahl", 2);
    if (request.getSession().getAttribute("ausgabeAccErst") == null) {
        request.getSession().setAttribute("ausgabeAccErst", "");
} 
    %>
</header>

<body>
    <nav>
        <ul>
            <li>
                <a> </a>
            </li>
        </ul>
    </nav>
    <form method="Post" action="${CP}/servlet/benutzer/anlegen" style="margin-left: 30%;">
        </br>
        <label id="benutzername" for="benutzername">Benutzername:</br>
            <input type="text" name="benutzername" placeholder="Benutzername" maxlength="30" required></br>
        </label>
        <label id="Email" for="email">Email-Adresse:</br>
            <input type="text" name="email" placeholder="me@example.com" maxlength="30" required></br>
        </label>
        <label for="passwort">Passwort:</br>
            <input type="password" id="passwort" name="passwort" placeholder="mindestens 8 Zeichen" minlength="8" required></br>
        </label>
        <label for="passwortb">Passwort wiederholen:</br>
            <input type="password" id="passwortb" name="passwortb" placeholder="Passwort wiederholen" minlength="8" required></br></br>
        </label>
        <%out.println("<p>" + request.getSession().getAttribute("ausgabeAccErst") + "</p>" );%>
        <br>
        <button id="registrieren"
            style="background-color:D22118; color:white; margin-left:1px; border-radius:20px; height: 30px; width: 180px; font-size:20px "
            type="submit" id="Login" name="Login" style="margin-top:20px">Registrieren</button>
    </form>
</body>
<style>
    label {
        font-family: monospace;
        font-size: 4vh;
    }

    nav {
        width: 100vw;
        background-color: #D22118;
    }

    ul {
        margin: 0;
        padding: 0;
        display: flex;
    }

    li {
        list-style-type: none;
        margin: 0 2vw;
        font-size: 4vh;
    }

    a {
        text-decoration: none;
        color: white;
        padding: 2vw;
        font-family: monospace;
    }
</style>