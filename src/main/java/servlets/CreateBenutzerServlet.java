package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;

public class CreateBenutzerServlet extends HttpServlet {
	
	
	DBManager d = new DBManager();
	RequestDispatcher rd;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {


		String benutzername = request.getParameter("benutzername");
		String email = request.getParameter("email");
		String passwort = request.getParameter("passwort");
		String passwortb = request.getParameter("passwortb");
		String fehler = "";
		

		if (passwort.equals(passwortb)) {
			if (d.getIDZuName(benutzername) != -1) {
				fehler = "Benutzername bereits vergeben";
				request.getSession().setAttribute("ausgabeAccErst", fehler);
				rd = request.getRequestDispatcher("/AccountErstellen.jsp");
				
			} else{
			d.benutzerAnlegen(benutzername, passwort, email, 0);
			request.getSession().setAttribute("ausgabe", null);
			rd = request.getRequestDispatcher("/index.jsp");
			
			}
			
			
		}

		else {
			fehler = "Die Passwoerter sind nicht gleich";
			request.getSession().setAttribute("ausgabeAccErst", fehler);
			rd = request.getRequestDispatcher("/AccountErstellen.jsp");
			
		}
		rd.forward(request, response);
	}
}
