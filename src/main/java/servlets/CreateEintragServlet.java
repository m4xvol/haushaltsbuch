package servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;

public class CreateEintragServlet extends HttpServlet {

	DBManager d = new DBManager();
	RequestDispatcher rd;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		

		int benutzerid = (Integer) request.getSession().getAttribute("id");
		
		if (benutzerid != -1) {

		String datum = request.getParameter("datum");
		
		String[] einzeln = datum.split("-");
		int tag = Integer.parseInt(einzeln[0]);
		int monat = Integer.parseInt(einzeln[1]);
		int jahr = Integer.parseInt(einzeln[2]);
		LocalDate dbdatum = LocalDate.of(tag, monat, jahr);
		System.out.println(dbdatum);
		
		String beschreibung = request.getParameter("beschreibung");
		double preis = Double.parseDouble(request.getParameter("preis"));
		String kategorieE = request.getParameter("kategorie");
		int kategorieBez = d.getIdKategorie(kategorieE, benutzerid);
		d.eintragErstellen(beschreibung, preis, dbdatum, kategorieBez, benutzerid);

		rd = request.getRequestDispatcher("/servlet/eintrag/laden");
		rd.forward(request, response);
		
		}
		
		else {
			rd = request.getRequestDispatcher("/index.jsp");
			rd.forward(request, response);
		}

	}
}
