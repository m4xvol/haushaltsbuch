<%@ page contentType="text/html; charset=UTF-8" %>

<head>
        <link rel="stylesheet" href="/stylesheet.css">
        <title>GDH - Anmelden</title>
        <link rel="icon" type="image/png" href="/img/hdg_icon.png">
</head>

<header>
        <div align="center"><img src="/img/banner.png"> </div>
</header>
<%
request.getSession().setAttribute("seitenzahl", 1);
request.getSession().setAttribute("id", -1);
if (request.getSession().getAttribute("ausgabe") == null) {
        request.getSession().setAttribute("ausgabe", "");
} 
%>

<body>

        <nav>
                <ul>
                        <li>
                                <a> </a>
                        </li>
                </ul>
        </nav>
        </div>

        <form method="Post" action="${CP}/servlet/benutzer/anmelden">

                <div align="center">
                        <font face="Verdana">
                                <label id="benutzername" for="benutzername">Benutzername:
                                        <input type="text" name="benutzername" placeholder="Benutzername" maxlength="30" required>
                                </label>

                                <label id="passwort" for="passwort">Passwort:
                                        <input type="password" name="passwort" placeholder="mindestens 8 Zeichen"
                                                minlength="5">
                                </label>
                                <button style="background-color:D22118; color:white; margin-left:1px; border-radius:20px; height: 30px; width: 100px; font-size:20px "
                                        type="submit" id="Login" name="Login" style="margin-top:20px">Login</button>
                                        <% out.println("<p>" + request.getSession().getAttribute("ausgabe") + "</p>"); %>
                        </font>
                </div>
                <div align="center" style="margin-top: 20px">

                        <a  id="login"></a>
                </div>
                <div align="center" style="margin-top: 20px">
                        <a style="color: blue;" href="/AccountErstellen.jsp" id="registrieren">Sie haben noch kein Account?
                                Erstellen Sie hier einen.</a>
                </div>
                <div id="logoindex">
                        <img id="kopf" src="/img/ein-bisschen-widerstand-mit.jpg">
                </div>
        </form>
</body>

<style>
nav > ul > li {
        position: relative;
        display: inline-block; 
      }
      nav > ul > li > ul {
        position: absolute;
        list-style-type: none;
        background-color: #D22118;
        display: none;
      }
      nav > ul > li:hover > ul { 
        display: block; 
      }
</style>