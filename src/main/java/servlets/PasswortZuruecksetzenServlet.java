package servlets;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;

public class PasswortZuruecksetzenServlet extends HttpServlet{
	
	int benutzerid;
	int zurueckZuSetzen;
	
	DBManager dbm = new DBManager();
	RequestDispatcher rd;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)throws ServletException,
	IOException{
		benutzerid =(Integer) request.getSession().getAttribute("id");
		zurueckZuSetzen = Integer.parseInt(request.getParameter("idpw"));
		String nameRueck = dbm.getNameZuId(zurueckZuSetzen);
		String email = dbm.getEmailZuId(zurueckZuSetzen);
		String neuesPasswort=dbm.passwortZuruecksetzen(zurueckZuSetzen);
		
		request.getSession().setAttribute("email", email);
		request.getSession().setAttribute("nameRueck", nameRueck);
		request.getSession().setAttribute("ausgabeNeuesPasswort", neuesPasswort);
	
		rd=request.getRequestDispatcher("/servlet/benutzer/laden");
		rd.forward(request, response);	
	}
}
