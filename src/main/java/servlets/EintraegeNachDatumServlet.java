package servlets;

import dbzugriff.*;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;
import java.util.ArrayList;
import java.time.*;

public class EintraegeNachDatumServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	int benutzerid;
	String adatumString;
	String bdatumString;

	DBManager dbm = new DBManager();
	RequestDispatcher rd;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// erstes Datum
		adatumString = request.getParameter("von");
		String[] aeinzeln = adatumString.split("-");
		int atag = Integer.parseInt(aeinzeln[0]);
		int amonat = Integer.parseInt(aeinzeln[1]);
		int ajahr = Integer.parseInt(aeinzeln[2]);
		LocalDate adatum = LocalDate.of(atag, amonat, ajahr);

		// zweites Datum
		bdatumString = request.getParameter("bis");
		String[] beinzeln = bdatumString.split("-");
		int btag = Integer.parseInt(beinzeln[0]);
		int bmonat = Integer.parseInt(beinzeln[1]);
		int bjahr = Integer.parseInt(beinzeln[2]);
		LocalDate bdatum = LocalDate.of(btag, bmonat, bjahr);
		
		double summe = 0;

		benutzerid = (Integer) request.getSession().getAttribute("id");
		if (adatum.compareTo(bdatum) <= 0) {
			ArrayList<Eintrag> eintraege = dbm.alleEintraegeAusgeben(benutzerid);
			ArrayList<Eintrag> ausgabeEintraege = new ArrayList<Eintrag>();
			for (Eintrag e : eintraege) {
				
				if (e.getAusgabedatum().compareTo(adatum) >= 0 && e.getAusgabedatum().compareTo(bdatum) <= 0) {
					ausgabeEintraege.add(e);
					summe = summe + e.getBetrag();
				} // if
			}//for
			if (!ausgabeEintraege.isEmpty()) {
				
				request.getSession().setAttribute("eintraegeNachDatum", ausgabeEintraege);
				request.getSession().setAttribute("antwort", "Zwischen dem "+ adatumString +" und dem "+ bdatumString + " wurden " + summe + " € ausgegeben");
				rd = request.getRequestDispatcher("/servlet/eintrag/laden");
			} // if
			else {
				request.getSession().setAttribute("antwort", "Keine Eintraege in diesem Zeitraum.");
				rd = request.getRequestDispatcher("/Startseite.jsp");
			}
		} // if

		else {
			request.getSession().setAttribute("antwort", "Das Anfangsdatum liegt nach dem Endedatum.");
			rd = request.getRequestDispatcher("/Startseite.jsp");
		} // else
		rd.forward(request, response);
	}//doPost
}
