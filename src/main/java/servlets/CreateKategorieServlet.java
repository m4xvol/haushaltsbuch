package servlets;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;

public class CreateKategorieServlet extends HttpServlet {

	DBManager d = new DBManager();
	RequestDispatcher rd;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		int benutzerid = (Integer) request.getSession().getAttribute("id");

		if (benutzerid != -1) {
			
		
		String kategorie=request.getParameter("kategorie");
		d.kategorieErstellen(kategorie, benutzerid);
		rd=request.getRequestDispatcher("/servlet/eintrag/laden");
		rd.forward(request, response);
		
		}
		else {
			rd = request.getRequestDispatcher("/index.jsp");
			rd.forward(request, response);
		}
	}

}
