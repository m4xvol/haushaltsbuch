package servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;

public class EintragAendernServlet extends HttpServlet{
	
	int benutzerid;
	DBManager dbm = new DBManager();
	RequestDispatcher rd;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	IOException{
		benutzerid=(Integer) request.getSession().getAttribute("id");
		
		if(benutzerid != -1) {
		
		String datum = request.getParameter("datum");

		System.out.println(datum);
		
		String[] einzeln = datum.split("-");
		int tag = Integer.parseInt(einzeln[0]);
		int monat = Integer.parseInt(einzeln[1]);
		int jahr = Integer.parseInt(einzeln[2]);
		LocalDate dbdatum = LocalDate.of(tag, monat, jahr);
		System.out.println(dbdatum);
		
		
		String kategorieU=request.getParameter("kategorie");
		int kategorie = dbm.getIdKategorie(kategorieU, benutzerid);
		int eid= (Integer)request.getSession().getAttribute("eintragid");
		String bez=request.getParameter("bez");
		double betrag = Double.parseDouble(request.getParameter("betrag"));
		dbm.eintragAendern(eid, bez, betrag, dbdatum , kategorie);
		
		rd=request.getRequestDispatcher("/servlet/eintrag/laden");
		rd.forward(request, response);
		
		}
		
		else {
			rd = request.getRequestDispatcher("/index.jsp");
			rd.forward(request, response);
		}
	}

}
