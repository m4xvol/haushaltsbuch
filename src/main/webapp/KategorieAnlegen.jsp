<%@ page contentType="text/html; charset=UTF-8" %>

<head>
  <link rel="stylesheet" href="stylesheet.css">
  <link rel="icon" type="image/png" href="/img/hdg_icon.png">
  <title>GDH - Kategorie anlegen</title>
</head>
<header>
  <div align="center"><img src="/img/banner.png"> </div>

</header>

<body>
  <% request.getSession().setAttribute("seitenzahl", 5);%>


  <div id="menu">
    <nav>
      <ul>
        <li class="topmenue">
            <form method="POST" action="/servlet/eintrag/laden">                     
              <button 
                type="submit" id="Startseite" name="Startseite" >Startseite</button>
            </form>
        </li>

        <li class="topmenue">
          <a herf="#Bearbeitung"> Bearbeitung </a>
          <ul>
            <li class="submenu"><a href="/EintragAnlegen.jsp">Neuer Eintrag</a></li>
            <li class="submenu"><a href="/KategorieAnlegen.jsp">Neue Kategorie</a></li>
          </ul>
        </li>

        <li class="topmenue">
          <a href="#Profil"> <img src="/img/icons8-user-20.png">Profil </a>
          <ul>
            <li class="submenu">
              <form method="POST" action="/servlet/benutzer/abmelden">                     
              <button 
                type="submit" id="Abmelden" name="Abmelden" style="margin-top:20px">Abmelden</button>
                </form>
              </li>
            <li class="class=submenu"><a href="/PWAendern.jsp">neues Passwort</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>


  <form id="anlegenform" method="Post" action="servlet/kategorie/anlegen">
    </br>
    <label id="kategorie" for="kategorie">Neue Kategorie:</br>
      <input type="text" name="kategorie" required></br>
    </label></br>
    <button style="background-color:D22118; color:white; border-radius:20px; height: 30px; width: 200px; font-size:20px"
      type="submit" value="Kategorie anlegen">Kategorie anlegen
    </button>
  </form>

  <div id="logo">
    <img src="/img/ein-bisschen-widerstand-mit.jpg" width="350" height="350" style="margin-top: 35%;">
  </div>

</body>

<style>
  form#anlegenform {
    margin-left: 30%;
    margin-top: 5%;
  }

  label {
    font-family: monospace;
    font-size: 4vh;
  }
  button{
    background-color:D22118; 
    color:white; 
    font-size: 4vh; 
    border: none; 
    font-family: monospace;
  }
  button:hover{
    color: blue;
    cursor: pointer;
  }
</style>