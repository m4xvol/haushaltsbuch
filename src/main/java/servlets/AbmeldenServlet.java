package servlets;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class AbmeldenServlet extends HttpServlet{
	
	RequestDispatcher rd;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	IOException{
		
		request.setAttribute("id", -1);
		request.getSession().setAttribute("email", null);
		request.getSession().setAttribute("nameRueck", null);
		request.getSession().setAttribute("ausgabeNeuesPasswort", null);
		
		rd=request.getRequestDispatcher("/index.jsp");
		rd.forward(request, response);
		
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request,response);
	}

}
