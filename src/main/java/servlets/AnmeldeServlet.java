package servlets;

import java.io.IOException;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;

public class AnmeldeServlet extends HttpServlet
{
	String benutzername;
	String passwort;
	int benutzerid;
	int iderhalten;
	String ausgabe;
	RequestDispatcher rd;
	
	DBManager dbm= new DBManager();
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		benutzername= request.getParameter("benutzername");
		passwort= request.getParameter("passwort");
		int pwhash = passwort.hashCode();
		
		int angelegt= dbm.getIDZuName(benutzername);
				
				if(angelegt!=-1)
				{
					benutzerid=dbm.getIDZuName(benutzername);
				}
				if(pwhash != dbm.passwortAbfragen(benutzername))
				{
					String fehler= "Das Passwort stimmt nicht mit dem Benutzernamen ueberein." 
							+" Bei Fragen wenden Sie sich an Ihren Administrator.";
					request.getSession().setAttribute("ausgabe", fehler);

				    rd = request.getRequestDispatcher("/index.jsp");
					rd.forward(request, response);
				}
				else {
					request.getSession().setAttribute("id", benutzerid);
					request.getSession().setAttribute("ausgabe", null);
					if(dbm.istAdmin(benutzername)==1) {
						rd = request.getRequestDispatcher("/servlet/benutzer/laden");
						rd.forward(request, response);
					}else {		
						rd = request.getRequestDispatcher("/servlet/eintrag/laden");			
						rd.forward(request, response);
					}//else
				}//else
	}//doPost
}//class