package servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;
import dbzugriff.Eintrag;


public class UpdateServlet extends HttpServlet{
	
	int benutzerid;
	DBManager dbm = new DBManager();
	RequestDispatcher rd;
		

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws
	ServletException, IOException{
		
		benutzerid=(Integer) request.getSession().getAttribute("id");
		int aufgerufeneSeite = (Integer)(request.getSession().getAttribute("seitenzahl"));
		
		switch(aufgerufeneSeite)
		{
		case 3:
			dbm.passwortZuruecksetzen(benutzerid);
			rd=request.getRequestDispatcher("/Admin.jsp");
			
		case 7:
			String passwort;
			String passwortkontrolle;
			passwort=request.getParameter("passwort");
			passwortkontrolle=request.getParameter("passwortkontrole");
			if(passwort.equals(passwortkontrolle))
			{
				dbm.passwortAendern(benutzerid, passwort);
				rd=request.getRequestDispatcher("/Startseite.jsp");
				rd.forward(request, response);
				
			}else 
			{
				String fehler="FEHLER!! "+
							  "Die eingegebenen Passwörter stimmen nicht überein. Bitte versuchen "+
			                  "Sie es erneut.";
				
				
				request.setAttribute("ausgabe", fehler);
				rd=request.getRequestDispatcher("/PasswortAenderung.jsp");
				rd.forward(request, response);
			}
		case 6:

			String datum = request.getParameter("datum");
			DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("YYYY-MM-DD");
			LocalDate localdate = LocalDate.parse(datum, localdateformatter);
			String kategorieU=request.getParameter("kategorie");
			int kategorie = dbm.getIdKategorie(kategorieU, benutzerid);
			int eid= Integer.parseInt(request.getParameter("eid"));
			String bez=request.getParameter("bez");
			double betrag = Double.parseDouble(request.getParameter("betrag"));
			dbm.eintragAendern(eid, bez, betrag, localdate, kategorie);
			
			rd=request.getRequestDispatcher("/Startseite.jsp");
			rd.forward(request, response);
		}
		
		
		
		
}
}