package servlets;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;

public class CreateServlet extends HttpServlet {

	DBManager d = new DBManager();
	RequestDispatcher rd;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		int benutzerid = (Integer) request.getSession().getAttribute("id");
		
		int aufgerufeneSeite = (Integer)(request.getSession().getAttribute("seitenzahl"));

		switch (aufgerufeneSeite) {

		case 2:// Nutzer anlegen

			String benutzername = request.getParameter("benutzername");
			String email = request.getParameter("email");
			String passwort = request.getParameter("passwort");
			String passwortb = request.getParameter("passwortb");

			if (passwort == passwortb) {
				if (d.getIDZuName(benutzername) != -1) {
					String fehler = "Benutzername bereits vergeben";
					request.setAttribute("ausgabe", fehler);
					rd = request.getRequestDispatcher("/AccountErstellen.jsp");
				}
				d.benutzerAnlegen(benutzername, passwort, email, 0);
				rd=request.getRequestDispatcher("/index.jsp");
				rd.forward(request, response);
			} 
			
			else {
				String fehler = "Die Passwoerter sind nicht gleich";
				request.setAttribute("ausgabe", fehler);
				rd = request.getRequestDispatcher("/AccountErstellen.jsp");
				rd.forward(request, response);
			}

		case 5:// Kategorie anlegen
			
			String kategorie=request.getParameter("kategorie");
			d.kategorieErstellen(kategorie, benutzerid);
			rd=request.getRequestDispatcher("/Startseite.jsp");
			rd.forward(request, response);


		case 4:// Eintrag anlegen
			
			String datum = request.getParameter("datum");
			DateTimeFormatter localdateformatter = DateTimeFormatter.ofPattern("YYYY-MM-DD");
			LocalDate localdate = LocalDate.parse(datum, localdateformatter);
			String beschreibung=request.getParameter("beschreibung");
			double preis=Double.parseDouble(request.getParameter("preis"));
			String kategorieE=request.getParameter("kategorie");
			int kategorieBez = d.getIdKategorie(kategorieE, benutzerid);
			d.eintragErstellen(beschreibung, preis, localdate, kategorieBez, benutzerid);
			
			rd = request.getRequestDispatcher("/Startseite.jsp");
			rd.forward(request, response);
			


		}
	}

}
