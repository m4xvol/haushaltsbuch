package servlets;

import java.io.IOException;
import java.time.LocalDate;
import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;
import dbzugriff.Eintrag;

public class EintragAendernLadenServlet extends HttpServlet{
	
	int benutzerid;
	int eintragid;
	LocalDate datum;
	String datumString;
	Eintrag e;
	DBManager dbm = new DBManager();
	RequestDispatcher rd;
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,
	IOException{
		benutzerid=(Integer) request.getSession().getAttribute("id");
		eintragid = Integer.parseInt(request.getParameter("eintragid"));
		request.getSession().setAttribute("eintragid", eintragid);
		
		if (benutzerid !=-1) {
		
		e = dbm.getEintrag(eintragid);
		
		datum = e.getAusgabedatum();
		String Tag = Integer.toString(datum.getDayOfMonth());
		if(Tag.length()==1){
			Tag = "0"+Tag;
		}
		String Monat = Integer.toString(datum.getMonthValue());
		if(Monat.length()==1) {
			Monat = "0"+Monat;
		}
		String Jahr = Integer.toString(datum.getYear());
		
		datumString = Jahr + "-" + Monat + "-" + Tag;
		
		request.getSession().setAttribute("bez", e.getBezeichnung());
		request.getSession().setAttribute("betrag", e.getBetrag());
		request.getSession().setAttribute("ausgabedatum", datumString);
		request.getSession().setAttribute("kategorie", dbm.getBezKategorie(e.getKategorieID()));
		
		rd=request.getRequestDispatcher("/EintragAendern.jsp");
		rd.forward(request, response);
		
		}
		else {
			rd = request.getRequestDispatcher("/index.jsp");
			rd.forward(request, response);
		}
	}

}