<%@ page import="java.util.*" %>
<%@ page import="dbzugriff.*" %>
<%@ page import="servlets.*" %>
<%@ page contentType="text/html; charset=UTF-8" %>

<% if (request.getSession().getAttribute("email") == null) {
  request.getSession().setAttribute("email", "");
  request.getSession().setAttribute("nameRueck", "");
  request.getSession().setAttribute("ausgabeNeuesPasswort", "");
}%>
<head>
  <link rel="icon" type="image/png" href="/img/hdg_icon.png">
  <title>HDG - Admin</title>
</head>
<header>
  <div align="center"><img src="/img/banner.png"> </div>
</header>

<body>

  <nav>
    <ul>

      <li id="logout">
        <form method="POST" action="/servlet/benutzer/abmelden">
          <button 
            type="submit" id="Abmelden" name="Abmelden">Abmelden</button>
        </form>
      </li>
    </ul>
  </nav>
  <table style="margin-left: auto; margin-right: auto; margin-top: 5px;">
    <thead>
      <tr>
        <th>
          Benutzer
        </th>
        <th>
          E-mail
        </th>
        <th>
          Passwort</br>
          ruecksetzen
        </th>
        <th>
          Benutzer</br>löschen
        </th>

      </tr>
    </thead>
    <% 
    DBManager dbm = new DBManager();
    ArrayList<Benutzer> bestehendeBenutzer = (ArrayList)request.getSession().getAttribute("Benutzer");
    for(Benutzer b : bestehendeBenutzer){   
        out.println("<tr><td>"+ b.getName() +"</td><td>"+ b.getEmail() +"</td><td><form method='POST' action='/servlet/passwort/zuruecksetzen'><button type='submit' id='pwruecksetzen'>P</button><input type='hidden' name='idpw' value='" + b.getId() + "'></form></td><td><form method='POST' action='/servlet/benutzer/loeschen'><button type='submit' id='bloeschen'>3</button><input type='hidden' name='nutzerid' value='" + b.getId() + "'></form></td></tr>");
    }
  %>

  </table>
  <table style="margin-left: auto; margin-right: auto; margin-top: 10px;">
  <thead>
    <tr>
      <th>
          E-Mail an <% out.println(request.getSession().getAttribute("email")); %>:
      </th>
    </tr>
  </thead>
    <tr>
      <td>
          <%out.println("<p>Hallo " + request.getSession().getAttribute("nameRueck") + "!</br></br>Auf Ihren Wunsch wurde Ihr Passwort zurueckgesetzt. </br> Ihr neues Passwort lautete:" + request.getSession().getAttribute("ausgabeNeuesPasswort") + "</br></br>Bitte ändern Sie Ihr Passwort nach der nächsten Anmeldung.</br></br>Ihr Haus des Geldes Team.</p>" );%>
      </td>
    </tr>
  </table>
</body>
<style>
  nav {
    width: 100vw;
    background-color: #D22118;
  }

  ul {
    margin: 0;
    padding: 0;
    display: flex;
  }

  li {
    list-style-type: none;
    margin: 0 2vw;
    font-size: 4vh;
  }

  a {
    text-decoration: none;

    color: white;
    padding: 2vw;
    font-family: monospace;
  }

  b {
    text-decoration: none;
    color: #D22118;
    padding: 2vw;
    font-family: monospace;
  }

  a:hover {
    color: blue;
  }

  table td {
    border: 1px solid black;
  }

  th {
    border: 1px solid black;
  }

  #pwruecksetzen {
    font-family: 'Wingdings 3';
  }

  #bloeschen {
    font-family: 'Wingdings 2';
  }
  button{
    background-color:D22118; 
    color:white; 
    font-size: 4vh; 
    border: none; 
    font-family: monospace;
  }
  button:hover{
    color: blue;
    cursor: pointer;
  }
</style>