package dbzugriff;

public class Kategorie {

	private int kid = -1;
	private String bezeichnung = null;
	private int benutzer= -1;
	
	Kategorie(){
		
	}
	
	Kategorie(int i, String bez, int ben){
		kid=i;
		bezeichnung = bez;
		benutzer = ben;
	}//Kategorie 
	
	public String getBezeichnung() {
		return bezeichnung;
	}
	public int getBenutzerID() {
		return benutzer;
	}
	public int getkid() {
		return kid;
	}
}
