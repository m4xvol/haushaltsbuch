package servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dbzugriff.DBManager;
import dbzugriff.Kategorie;


public class KategorieLadenServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	int benutzerid;

	DBManager dbm = new DBManager();
	RequestDispatcher rd;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		benutzerid = (Integer) request.getSession().getAttribute("id");

		ArrayList<Kategorie> kategorie = new ArrayList<Kategorie>();
		kategorie = dbm.alleKategorienAusgeben(benutzerid);
		request.getSession().setAttribute("Kategorien", kategorie);

		rd = request.getRequestDispatcher("/KategorieEinsehen.jsp");
		rd.forward(request, response);

	}

}
