<%@ page contentType="text/html; charset=UTF-8" %>

<header>
  <div align="center"><img src="/img/banner.png"> </div>
</header>
<head>
  <link rel="icon" type="image/png" href="/img/hdg_icon.png">
  <title>GDH - Eintrag anlegen</title>
</head>

<body>
  <%request.getSession().setAttribute("seitenzahl", 4);%>
  <div id="menu">
    <nav>
      <ul>
        <li class="topmenue">
          <form method="POST" action="/servlet/eintrag/laden">
            <button type="submit" id="Startseite" name="Startseite">Startseite</button>
          </form>
        </li>

        <li class="topmenue">
          <a herf="#Bearbeitung"> Bearbeitung </a>
          <ul>
            <li class="submenu"><a href="/EintragAnlegen.jsp">Neuer Eintrag</a></li>
            <li class="submenu"><a href="/KategorieAnlegen.jsp">Neue Kategorie</a></li>
          </ul>
        </li>
        <li class="topmenue">
          <a href="#Profil"> <img src="/img/icons8-user-20.png">Profil </a>
          <ul>
            <li class="submenu">
              <form method="POST" action="/servlet/benutzer/abmelden">
                <button type="submit" id="Abmelden" name="Abmelden" style="margin-top:20px">Abmelden</button>
              </form>
            </li>
            <li class="class=submenu"><a href="/PWAendern">neues Passwort</a></li>
          </ul>
        </li>
      </ul>
    </nav>
  </div>

  <form id="anlegenform" method="Post" action="servlet/eintrag/anlegen">

    <%request.getSession().setAttribute("seitenzahl", 4);%>

    <label id="datum" for="datum">Datum: </br>
      <input type="date" name="datum" placeholder="JJJJ-MM-TT" required></br>
    </label>
    <label id="beschreibung" for="beschreibung">Beschreibung: </br>
      <input type="text" name="beschreibung" placeholder="Beschreibung" maxlength="30" required></br>
    </label>
    <label id="preis" for="preis">Preis in €: </br>
      <input type="text" name="preis" placeholder="__.__EUR" required></br>
    </label>
    <label id="kategorie" for="kategorie">Kategorie: </br>
      <input type="search" name="kategorie" placeholder="Sonstiges" required></br>
    </label></br></br>
    <input type="submit" value="Eintrag anlegen"
      style="background-color:D22118; color:white; border-radius:20px; height: 30px; width: 200px; font-size:20px">
  </form>

  <div id="logo">
    <img src="/img/ein-bisschen-widerstand-mit.jpg" width="350" height="350" style="margin-top: 35%;">
  </div>
</body>

<style>
  #logo {
    position: absolute;
    top: 310px;
    left: 65%;
  }

  form#anlegenform {
    margin-left: 30%;
    margin-top: 20px;
  }

  label {
    font-family: monospace;
    font-size: 4vh;
  }

  #button1 {
    position: absolute;
    top: 520px;
    left: 47%;
    font-size: 5vh;
  }

  nav {
    width: 100vw;
    background-color: #D22118;
  }

  ul {
    margin: 0;
    padding: 0;
    display: flex;
  }

  li {
    list-style-type: none;
    margin: 0 2vw;
    font-size: 4vh;
  }

  a {
    text-decoration: none;
    color: white;
    padding: 2vw;
    font-family: monospace;
  }

  b {
    text-decoration: none;
    color: #D22118;
    padding: 2vw;
    font-family: monospace;
  }


  nav>ul>li {
    position: relative;
    display: inline-block;
  }

  nav>ul>li>ul {
    position: absolute;
    list-style-type: none;
    background-color: #D22118;
    display: none;
  }

  nav>ul>li:hover>ul {
    display: block;
  }

  a:hover {
    color: blue;
  }

  button {
    background-color: D22118;
    color: white;
    font-size: 4vh;
    border: none;
    font-family: monospace;
  }

  button:hover {
    color: blue;
    cursor: pointer;
  }
</style>